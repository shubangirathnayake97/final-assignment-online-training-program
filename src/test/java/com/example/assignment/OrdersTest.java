//package com.example.assignment;
//
//import com.example.assignment.entity.Orders;
//import com.example.assignment.repository.OrdersRepository;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
//import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
//import org.springframework.test.annotation.Rollback;
//
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//
//@DataJpaTest
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//public class OrdersTest {
//    @Autowired
//    private OrdersRepository ordersRepository;
//
//    @Test
//    @Rollback(false)
//    public void testAddOrders() {
//        Orders orders = new Orders();
//        Orders savedOrders = ordersRepository.save(orders);
//        assertNotNull(savedOrders);
//    }
//}
