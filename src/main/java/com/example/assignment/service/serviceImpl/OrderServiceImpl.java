package com.example.assignment.service.serviceImpl;

import com.example.assignment.entity.Orders;
import com.example.assignment.exceptions.RecordNotFoundException;
import com.example.assignment.repository.CompanyRepository;
import com.example.assignment.repository.OrdersRepository;
import com.example.assignment.service.OrderService;
import com.example.assignment.util.ExceptionMessages;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {
    private final CompanyRepository companyRepository;
    private final OrdersRepository ordersRepository;

    public OrderServiceImpl(CompanyRepository companyRepository, OrdersRepository ordersRepository) {
        this.companyRepository = companyRepository;
        this.ordersRepository = ordersRepository;
    }

    @Override
    public List<Orders> viewOrderHistory(String senderName) throws RecordNotFoundException {
        return new ArrayList<>(ordersRepository.findBySenderName(senderName)
                .orElseThrow(() -> new RecordNotFoundException(ExceptionMessages.USER_NOT_FOUND)));
    }

    @Override
    public Orders addOrders(Long companyId, Orders orders) throws RecordNotFoundException {
        return companyRepository.findById(companyId)
                .map(company -> {
                    orders.setCompany(company);
                    return ordersRepository.save(orders);
                })
                .orElseThrow(() -> new RecordNotFoundException(ExceptionMessages.COMPANY_NOT_FOUND));
    }

    @Override
    public Orders editOrders(Orders updatedOrders) throws RecordNotFoundException {
        return ordersRepository.findById(updatedOrders.getId())
                .map(orders -> {
                    orders.setParcelWeight(updatedOrders.getParcelWeight());
                    orders.setSenderName(updatedOrders.getSenderName());
                    orders.setSenderAddress(updatedOrders.getSenderAddress());
                    orders.setReceiverName(updatedOrders.getReceiverName());
                    orders.setReceiverAddress(updatedOrders.getReceiverAddress());
                    return ordersRepository.save(orders);
                })
                .orElseThrow(() -> new RecordNotFoundException(ExceptionMessages.ORDER_NOT_FOUND));
    }

    @Override
    public String deleteOrders(Long orderId) throws RecordNotFoundException {
        return ordersRepository.findById(orderId)
                .map(orders -> {
                    ordersRepository.delete(orders);
                    return ExceptionMessages.DELETED;
                }).orElseThrow(() -> new RecordNotFoundException(ExceptionMessages.ORDER_NOT_FOUND));
    }
}
