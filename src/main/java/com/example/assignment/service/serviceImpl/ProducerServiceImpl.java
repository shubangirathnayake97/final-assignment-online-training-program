package com.example.assignment.service.serviceImpl;

import com.example.assignment.entity.Company;
import com.example.assignment.exceptions.RecordNotFoundException;
import com.example.assignment.repository.CompanyRepository;
import com.example.assignment.service.ProducerService;
import com.example.assignment.util.ExceptionMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class ProducerServiceImpl implements ProducerService {
    public static final String topic = "topic";
    private final KafkaTemplate<String, String> kafkaTemplate;
    private final JavaMailSender javaMailSender;
    private final CompanyRepository companyRepository;

    @Autowired
    public ProducerServiceImpl(KafkaTemplate<String, String> kafkaTemplate, JavaMailSender javaMailSender, CompanyRepository companyRepository) {
        this.javaMailSender = javaMailSender;
        this.kafkaTemplate = kafkaTemplate;
        this.companyRepository = companyRepository;
    }

    public void publishToTopics(String message, String subject, Long customerCode) throws RecordNotFoundException {
        sendEmail(message, subject, customerCode);
    }

    private void sendEmail(String message, String subject, Long customerCode) throws RecordNotFoundException {
        SimpleMailMessage mail = new SimpleMailMessage();
        mail.setFrom("shubangirathnayake97@gmail.com");
        mail.setTo(companyRepository.findById(customerCode).map(Company::getEmail).orElseThrow(() -> new RecordNotFoundException(ExceptionMessages.COMPANY_NOT_FOUND)));
        mail.setText(message);
        mail.setSubject(subject);
        javaMailSender.send(mail);
    }
}
