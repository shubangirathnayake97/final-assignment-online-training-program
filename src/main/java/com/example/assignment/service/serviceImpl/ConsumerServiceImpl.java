package com.example.assignment.service.serviceImpl;

import com.example.assignment.service.ConsumerService;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class ConsumerServiceImpl implements ConsumerService {
    @Override
    @KafkaListener(topics = "topic", groupId = "myGroup")
    public void consumerFromTopic(String message) {
        System.out.println(message);
    }
}
