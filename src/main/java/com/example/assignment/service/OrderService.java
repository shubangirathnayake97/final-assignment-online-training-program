package com.example.assignment.service;

import com.example.assignment.entity.Orders;
import com.example.assignment.exceptions.RecordNotFoundException;

import java.util.List;

public interface OrderService {
    List<Orders> viewOrderHistory(String senderName) throws RecordNotFoundException;

    Orders addOrders(Long companyId, Orders orders) throws RecordNotFoundException;

    Orders editOrders(Orders orders) throws RecordNotFoundException;

    String deleteOrders(Long orderId) throws RecordNotFoundException;
}
