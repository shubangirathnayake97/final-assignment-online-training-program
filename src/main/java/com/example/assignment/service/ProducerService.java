package com.example.assignment.service;

import com.example.assignment.exceptions.RecordNotFoundException;

public interface ProducerService {
    void publishToTopics(String message, String subject, Long companyCode) throws RecordNotFoundException;
}
