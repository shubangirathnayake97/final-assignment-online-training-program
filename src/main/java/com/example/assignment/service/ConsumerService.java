package com.example.assignment.service;

import org.springframework.kafka.annotation.KafkaListener;

public interface ConsumerService {
    @KafkaListener(topics = "topic", groupId = "myGroup")
    void consumerFromTopic(String message);
}
