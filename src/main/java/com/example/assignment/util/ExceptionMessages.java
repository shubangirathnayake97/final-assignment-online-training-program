package com.example.assignment.util;

public class ExceptionMessages {
    public static final String COMPANY_NOT_FOUND = "Company not found";
    public static final String ORDER_NOT_FOUND = "Order not found";
    public static final String USER_NOT_FOUND = "User not found";
    public static final String DELETED = "Deleted successfully";
}
