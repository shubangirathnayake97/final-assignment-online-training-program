package com.example.assignment.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long companyCode;
    private String name;
    private String email;
    private String address;
    @OneToMany(mappedBy = "company", fetch = FetchType.LAZY)
    private List<Orders> orderSet;
}
