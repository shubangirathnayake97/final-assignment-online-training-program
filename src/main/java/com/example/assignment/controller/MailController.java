package com.example.assignment.controller;

import com.example.assignment.exceptions.RecordNotFoundException;
import com.example.assignment.service.ProducerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("api/v1/email")
public class MailController {
    private static final String SUCCESS_MESSAGE = "Mail sent successfully";
    private final ProducerService producerService;

    @Autowired
    public MailController(ProducerService producerService) {
        this.producerService = producerService;
    }

    @PostMapping("/send")
    public ResponseEntity<?> sendMessage(@RequestParam String message, @RequestParam String subject, @RequestParam Long companyCode) {
        try {
            producerService.publishToTopics(message, subject, companyCode);
            return ResponseEntity.ok(SUCCESS_MESSAGE);
        } catch (RecordNotFoundException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
