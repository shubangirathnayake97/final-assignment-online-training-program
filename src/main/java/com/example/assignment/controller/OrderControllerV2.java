package com.example.assignment.controller;

import com.example.assignment.entity.Orders;
import com.example.assignment.exceptions.RecordNotFoundException;
import com.example.assignment.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/v2/order")
public class OrderControllerV2 {
    private final OrderService orderService;

    @Autowired
    public OrderControllerV2(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("/add/{companyId}")
    public ResponseEntity<?> addOrders(@PathVariable Long companyId, @RequestBody Orders orders) {
        try {
            return ResponseEntity.ok(orderService.addOrders(companyId, orders));
        } catch (RecordNotFoundException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/history/{senderName}")
    public ResponseEntity<?> viewOrderHistory(@PathVariable String senderName) {

        try {
            return ResponseEntity.ok(orderService.viewOrderHistory(senderName));
        } catch (RecordNotFoundException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PutMapping("/edit")
    public ResponseEntity<?> editOrders(@RequestBody Orders orders) {

        try {
            return ResponseEntity.ok(orderService.editOrders(orders));
        } catch (RecordNotFoundException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @DeleteMapping("/delete/{orderId}")
    public ResponseEntity<?> deleteOrders(@PathVariable Long orderId) {

        try {
            return ResponseEntity.ok(orderService.deleteOrders(orderId));
        } catch (RecordNotFoundException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}
