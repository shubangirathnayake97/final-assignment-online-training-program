package com.example.assignment.repository;

import com.example.assignment.entity.Orders;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface OrdersRepository extends JpaRepository<Orders, Long> {
    Optional<List<Orders>> findBySenderName(String senderName);
}
